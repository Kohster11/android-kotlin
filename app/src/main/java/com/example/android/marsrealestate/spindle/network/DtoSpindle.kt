package com.example.android.marsrealestate.spindle.network



import com.squareup.moshi.JsonClass


@JsonClass(generateAdapter = true)
data class NetworkSpindleContant(val content: List<NetworkSpindle>)


@JsonClass(generateAdapter = true)
data class NetworkSpindle(
        val model: String,
        val manufacturer: String,
        val url: String,
        val country: String,
        val machineLocation: String,
        val description: String)


//fun NetworkSpindleContant.asDatabaseModel(): List<DatabaseVideo> {
//    return content.map {
//        DatabaseVideo(
//                title = it.model,
//                description = it.manufacturer,
//                url = it.url,
//                updated = it.country,
//                thumbnail = it.country)
//    }
//}

