package com.example.android.marsrealestate.lathe

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import kotlinx.coroutines.Deferred
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query


private const val BASE_URL = "http://q11.jvmhost.net"

private val moshi = Moshi.Builder()
        .add(KotlinJsonAdapterFactory())
        .build()

private val retrofit = Retrofit.Builder()
        .addConverterFactory(MoshiConverterFactory.create(moshi))
        .addCallAdapterFactory(CoroutineCallAdapterFactory())
        .baseUrl(BASE_URL)
        .build()


interface LatheApiService {
    @GET("lathe_json")
    fun getProperties(): Deferred<List<Lathe>>
//        fun getProperties(@Query("axes") type: String): Deferred<List<Lathe>> //Call<String>

}


object LatheApi {
    val retrofitService: LatheApiService by lazy {
        retrofit.create(LatheApiService::class.java)
    }
}